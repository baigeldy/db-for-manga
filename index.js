import express from "express";
import fetch from "node-fetch";
import fs from "fs";
import cors from "cors";

const app = express();
const port = process.env.PORT || 2222;

const authOptions = {
  noCors: false,
  static: "./public",
};

app.use(express.json());
app.use(cors());

app.get("/fetch-data", async (req, res) => {
  try {
    const response = await fetch("http://127.0.0.1:8000/api/");
    const data = await response.json();
    fs.writeFileSync("db.json", JSON.stringify(data, null, 2));
    res.json(data);
  } catch (error) {
    console.error("Error fetching data:", error);
    res.status(500).json({ error: "Error fetching data" });
  }
});

app.use(express.static(authOptions.static));

app.listen(port, () => {
  console.log("JSON Server is running on port " + port);
});
